import logging
import sys

import numpy as np
import pymc3 as pm
import pandas as pd
import arviz as az

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger(__name__)


def logistic(K, r, t, C_0):
    A = (K - C_0) / C_0
    return K / (1 + A * np.exp(-r * t))


# Load data
logger.info("Loading data")
df = pd.read_csv(
    "https://opendata.ecdc.europa.eu/covid19/casedistribution/csv/",
    parse_dates=["dateRep"],
    infer_datetime_format=True,
    dayfirst=True,
)

logger.info("Cleaning data")
df = df.rename(
    columns={"dateRep": "date", "countriesAndTerritories": "country"}
)  # Sane column names
df = df.drop(["day", "month", "year", "geoId"], axis=1)  # Not required

# Create DF with sorted index
sorted_data = df.set_index(df["date"]).sort_index()
sorted_data["cumulative_cases"] = sorted_data.groupby(by="country")["cases"].cumsum()
sorted_data["cumulative_deaths"] = sorted_data.groupby(by="country")["cases"].cumsum()

# Filter out data with less than 500 cases, we probably can't get very good estimates from these.
sorted_data = sorted_data[sorted_data["cumulative_cases"] >= 500]

# Remove "Czechia" it has a population of NaN
sorted_data = sorted_data[sorted_data["country"] != "Czechia"]

# Get final list of countries
countries = sorted_data["country"].unique()
n_countries = len(countries)

# Pull out population size per country
populations = {
    country: df[df["country"] == country].iloc[0]["popData2018"]
    for country in countries
}

# A map from country to integer index (for the model)
idx_country = pd.Index(countries).get_indexer(sorted_data.country)

# Create a new column with the number of days since first infection (the x-axis)
country_first_dates = {
    c: sorted_data[sorted_data["country"] == c].index.min() for c in countries
}
sorted_data["100_cases"] = sorted_data.apply(
    lambda x: country_first_dates[x.country], axis=1
)
sorted_data["days_since_100_cases"] = (
    sorted_data.index - sorted_data["100_cases"]
).apply(lambda x: x.days)


def model(x, y, index):

    hierarchical_model = pm.Model("Hierarchical Model")
    with hierarchical_model:
        BoundedNormal = pm.Bound(pm.Normal, lower=0.0)
        t = pm.Data("x_data", x)
        confirmed_cases = pm.Data("y_data", y)

        # Intercept - We fixed this at 100.
        C_0 = pm.Normal("C_0", mu=100, sigma=10)

        # Growth rate: 0.2 is approx value reported by others
        r_mu = pm.Normal("r_mu", mu=0.2, sigma=0.1)
        r_sigma = pm.HalfNormal("r_sigma", 0.5)
        r = BoundedNormal("r", mu=r_mu, sigma=r_sigma, shape=n_countries)

        # Total number of cases. Depends on the population, more people, more infections.
        K_mu = pm.Normal("K_mu", mu=30000, sigma=30000)
        K_sigma = pm.HalfNormal("K_sigma", 1000)
        K = pm.Gamma("K", mu=K_mu, sigma=K_sigma, shape=n_countries)

        # Logistic regression
        growth = logistic(K[index], r[index], t, C_0)

        # Likelihood error
        eps = pm.HalfNormal("eps")

        # Likelihood - Counts here, so poission or negative binomial. Causes issues. Lognormal tends to work better?
        pm.Lognormal("cases", mu=np.log(growth), sigma=eps, observed=confirmed_cases)
    return hierarchical_model


logger.info("Creating training model")
train_model = model(
    x=sorted_data["days_since_100_cases"],
    y=sorted_data["cumulative_cases"],
    index=idx_country,
)
logger.info("Training...")
with train_model:
    trace = pm.sample()

logger.info("Creating inference model")
n_days = 365  # Daily predictions

# Remember this is one big vector that contains all countries at all times.
# To do inference we need to construct a new vector with new times
# Create the time index
time_index = np.arange(0, n_days, 1)
time_index = np.repeat(time_index, n_countries)

# Create the country index
country_index = np.arange(n_countries)
country_index = np.tile(country_index, n_days)
dummy_y = np.zeros(len(time_index))

# Generate the inference model
inference_model = model(x=time_index, y=dummy_y, index=country_index)

# Train
logger.info("Inferencing...")
with inference_model:
    posterior = pm.sample_posterior_predictive(trace)

logger.info("Saving model")

# Calculate credible interval
credible_interval = az.hpd(
    posterior["Hierarchical Model_cases"], credible_interval=0.95
)

# Calculate dates (must be in python datetime to work with pydantic)
country_start = [country_first_dates[x] for x in countries[country_index].tolist()]
country_offset = [pd.DateOffset(x) for x in time_index]
dates = list(
    map(lambda x: (x[0] + x[1]).to_pydatetime(), zip(country_start, country_offset))
)

# Create a big dataframe with all this info
predictions = pd.DataFrame(
    {
        "timestamp": dates,
        "country": countries[country_index],
        "mean": np.mean(posterior["Hierarchical Model_cases"], axis=0),
        "median": np.median(posterior["Hierarchical Model_cases"], axis=0),
        "credible_interval_low": credible_interval[:, 0],
        "credible_interval_high": credible_interval[:, 1],
        "days_since_100_cases": time_index,
    },
    index=dates,
)

# Merge in the ground truth
predictions = pd.merge(
    predictions.rename_axis("index").reset_index(),
    sorted_data[["country", "cases", "cumulative_cases"]]
    .rename_axis("index")
    .reset_index(),
    on=["index", "country"],
    how="outer",
).set_index("index")

# Save to file
predictions.to_pickle("predictions.pkl")
logger.info("Finished")
